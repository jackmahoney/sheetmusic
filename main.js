var canvas = document.getElementById('canvas');
var renderer = new Vex.Flow.Renderer(canvas, Vex.Flow.Renderer.Backends.CANVAS);
var ctx = renderer.getContext();

function randIndex(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

function randomInt(min, max){
    return Math.floor(Math.random()*(max-min+1)+min);
}

function generateKeys(num, min, max) {
  var res = [];
  var chars = 'abcdefg'.split('');
  while(num > 0){
	res.push(randIndex(chars) + '/' + randomInt(min, max));  
        num--;
  }
  return res;
}

function staves(keys, duration) {
  return keys.map(function(key){
    return new Vex.Flow.StaveNote({ keys: [key], duration: duration });
  });
}

function renderStave(keys, stave) {
  // Create the notes
  var notes = staves(keys, "q");

  // Create a voice in 4/4
  var voice = new Vex.Flow.Voice({
  num_beats: 4,
  beat_value: 4,
  resolution: Vex.Flow.RESOLUTION
  });

  // Add notes to voice
  voice.addTickables(notes);

  // Format and justify the notes to 500 pixels
  var formatter = new Vex.Flow.Formatter().
  joinVoices([voice]).format([voice], 500);

  // Render voice
  voice.draw(ctx, stave);
}



function prepare() {
  ctx.clearRect(0,0,canvas.width, canvas.height);
  var stave = new Vex.Flow.Stave(10, 0, 500);
  stave.addClef("treble").setContext(ctx).draw();
  return stave;
}

function setColor(color){
  input.style.backgroundColor = color;
}

function init() {
  input.value = '';
  setColor('white');
  keys = generateKeys(4,4,5);
  renderStave(keys, prepare());  
  input.focus();
  console.log(keys);
}

var minAvInput = document.getElementById('min-average');
window.MIN_AVERAGE = 5;
minAvInput.value = window.MIN_AVERAGE;
minAvInput.onchange = function(e){
  window.MIN_AVERAGE = e.target.value;
};


var keys;
var input = document.getElementById('input');
input.addEventListener('keyup', handleInput);

var currentlyError = false;
function handleInput(){

  if(currentlyError) {
    var lastLetter = input.value[input.value.length - 1] || '';
    input.value = input.value.substr(0, input.value.length - 2) + lastLetter;
  }

  var correct = input.value === keys.map(function(key){ return key.split('/')[0] }).join('').substr(0, input.value.length);
  setColor(correct ? 'white' : '#ff5b5b');
  currentlyError = !correct;
  if(correct && input.value.length === keys.length){
    init();
  }
}

var lastNote;
window.addEventListener('note-captured', function(e){
  var note = e.detail.note;
  if (note) {
    note = note[0].toLowerCase();
    if(note !== lastNote){
      lastNote = note;
      input.value = input.value + note;
      handleInput();
    }
  }
});

// timer
(function(){
  var display = document.getElementById('timer');
  var start = Date.now();
  
  (function updateTimer(){
    var elapsedSeconds = Math.floor((Date.now() - start) / 1000);
    var minutes = Math.floor(elapsedSeconds / 60);
    var seconds = elapsedSeconds % 60;
    display.innerText = minutes +'m ' + seconds + 's';
    window.requestAnimationFrame(updateTimer);  
  })();

})();

init();



